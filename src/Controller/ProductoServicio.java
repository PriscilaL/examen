/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author Jadpa06
 */
public class ProductoServicio {
    public String saveProfilePhoto(File imagePath) throws IOException {
        String directoryPath = "src/img";

        File dir = new File(directoryPath);
        File newImage;
        if (dir.exists()) {
            directoryPath = directoryPath + "/" + imagePath.getName();
            newImage = new File(directoryPath);
        } else {
            directoryPath = "profilePicture";
            dir = new File(directoryPath);
            dir.mkdir();
            directoryPath = directoryPath + "/" + imagePath.getName();
            newImage = new File(directoryPath);
        }

        if (!newImage.exists()) {
            newImage.createNewFile();
        }

        try (FileInputStream fis = new FileInputStream(imagePath)) {
            byte[] byteImage = new byte[(int) imagePath.length()];
            fis.read(byteImage);
            FileOutputStream fos = new FileOutputStream(newImage);
            fos.write(byteImage);
        }

        return directoryPath;
    }

    public ImageIcon openProfilePhoto(String path) throws MalformedURLException {
        ImageIcon image;
        String filePath;
        if (path == null) {            
            image = new ImageIcon(getClass().getResource("/img/profileTemplate.png"));
            return image;
        }
        
        URL url = getClass().getResource(path);
        
        if (url == null) {            
            filePath = path;
        } else {
            
            return new ImageIcon(url);
        }
        
        image = new ImageIcon(filePath);

        return image;
    }
}
